import networkx as nx
import copy

from .abstractmolecule import AbstractMolecule


class Reaction:
    def __init__(self):
        self.reagents = {}
        self.lowmol_reagents = {}
        self.products = {}

    def check_reactivity(self, molecule):
        res = {}
        for reag_type, reag_pattern in self.reagents.items():
            res[reag_type] = molecule.match_pattern(reag_pattern)
        return res

    def get_reverse(self):
        new_reaction = Reaction()
        new_reaction.reagents = copy.deepcopy(self.products)
        new_reaction.products = copy.deepcopy(self.reagents)
        return new_reaction
    
    def get_products(self, start_molecules, settings):
        # Here, counting with 'mol_idx' is unified across different intermediates
        # 'mol_idx' are later used as indices to differentiate molecular graphs of reagents
        # (same 'mol_idx' <=> same reagent)
        next_mol = 0
        reagent_counters = {}
        for item in start_molecules.values():
            if item['intermediate_idx'] not in reagent_counters:
                reagent_counters[item['intermediate_idx']] = {}
            if item['mol_idx'] not in reagent_counters[item['intermediate_idx']]:
                reagent_counters[item['intermediate_idx']][item['mol_idx']] = next_mol
                next_mol += 1
            item['mol_idx'] = reagent_counters[item['intermediate_idx']][item['mol_idx']]
        
        for key, value in self.lowmol_reagents.items():
            start_molecules[key] = copy.deepcopy(value)
        
        mol_graphs = {}
        full_map = {}
        for data in start_molecules.values():
            mol_graphs[data['mol_idx']] = copy.deepcopy(data['molecule'].G)
            # Names of atoms should be unique
            node_map = {}
            for node in mol_graphs[data['mol_idx']]:
                node_map[node] = f"{data['mol_idx']}_{node}"
            mol_graphs[data['mol_idx']] = nx.relabel_nodes(mol_graphs[data['mol_idx']], node_map)
            data['center'] = {value: node_map[old_name] for old_name, value in data['center'].items()}
            for key, value in data['center'].items():
                full_map[key] = value
        
        total_graph = nx.Graph()
        for graph in mol_graphs.values():
            total_graph = nx.compose(total_graph, graph)
        
        # Need to backup types of removed bonds (in case they will be restored) <- This is questionable
        for reag_graph in self.reagents.values():
            for u, v in reag_graph.edges:
                if total_graph.has_edge(full_map[u], full_map[v]):
                    total_graph.remove_edge(full_map[u], full_map[v])
                elif settings['overlap_policy'] == 'improvise':
                    raise NotImplemented # This mode must use bond order counting
                else:
                    raise RuntimeError("Attempting to remove non-existent bond. Likely, a bug.")

        for reag_graph in self.lowmol_reagents.values():
            for u, v in reag_graph['pattern'].edges:
                total_graph.remove_edge(full_map[u], full_map[v])

        for prod_graph in self.products.values():
            for u, v, attrs in prod_graph.edges(data=True):
                if not total_graph.has_edge(full_map[u], full_map[v]):
                    total_graph.add_edge(full_map[u],
                                         full_map[v], **attrs)
                # elif settings['overlap_policy'] == 'improvise':
                #     if 'order' in attrs:
                #         old_order = total_graph[full_map[u]][full_map[v]]['order']
                #     for key, value in attrs.items():
                #         total_graph[full_map[u]][full_map[v]][key] = value
                #     if 'order' in attrs:
                #         total_graph[full_map[u]][full_map[v]]['order'] += old_order # Sum of old and new orders
                #         assert total_graph[full_map[u]][full_map[v]]['order'] < 4.0, \
                #                         f"Got some weird bond order {total_graph[full_map[u]][full_map[v]]['order']}."\
                #                         "Probably a bug or strange rules imposed"
                else:
                    raise RuntimeError("Bond collision detected (looks like it's a bug)")
            for at, attrs in prod_graph.nodes(data=True):
                for key, value in attrs.items():
                    total_graph.nodes[full_map[at]][key] = value
        products = []
        for comp in nx.connected_components(total_graph):
            mygraph = total_graph.subgraph(comp).copy()
            mygraph = nx.convert_node_labels_to_integers(mygraph)
            products.append(AbstractMolecule(graph=mygraph))
        return products
