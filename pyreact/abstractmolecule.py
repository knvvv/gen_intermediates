from rdkit import Chem
from rdkit.Chem import AllChem
import networkx as nx
from networkx.algorithms import isomorphism
import copy

# 1. Ignore bonded fragments in 'get_molidxs' only if forming bonds overlaps
# 2. Parallel execution
# 3. Interesting behavior - if one of the products matches some of 'exclude_patterns' doesn't mean that other associated products won't be accepted


class AbstractMolecule:
    def __init__(self, smiles=None, graph=None):
        if smiles is not None:
            self.from_smiles(smiles)
        elif graph is not None:
            self.G = graph

    def from_smiles(self, smiles):
        self.G = nx.Graph()
        mol = Chem.MolFromSmiles(smiles)
        mol = Chem.AddHs(mol)
        for atom in mol.GetAtoms():
            idx = atom.GetIdx()
            sym = atom.GetSymbol()
            chrg = atom.GetFormalCharge()
            nrad = atom.GetNumRadicalElectrons()
            self.G.add_node(idx, sym=sym, chrg=chrg, nrad=nrad)

        for bond in mol.GetBonds():
            self.G.add_edge(bond.GetBeginAtomIdx(), bond.GetEndAtomIdx(), order=bond.GetBondTypeAsDouble())
    
    def get_rdkit_mol(self):
        m = Chem.Mol()
        mol = Chem.EditableMol(m)
        for atom in self.G.nodes:
            new_atom = Chem.Atom(self.G.nodes[atom]['sym'])
            new_atom.SetFormalCharge(self.G.nodes[atom]['chrg'])
            new_atom.SetNumRadicalElectrons(self.G.nodes[atom]['nrad'])
            new_idx = mol.AddAtom(new_atom)
            assert new_idx == atom
        
        for edge in self.G.edges:
            mol.AddBond(*edge, Chem.BondType(self.G[edge[0]][edge[1]]['order']))

        mol = mol.GetMol()
        Chem.SanitizeMol(mol)
        # mol = Chem.RemoveHs(mol)
        return mol

    def get_smiles(self):
        mol = Chem.RemoveHs(self.get_rdkit_mol())
        return Chem.MolToSmiles(mol)

    def get_2d_image(self):
        from IPython.display import display
        from rdkit.Chem import Draw
        mol = self.get_rdkit_mol()
        mol = Chem.RemoveHs(mol)
        AllChem.Compute2DCoords(mol)
        image = Draw.MolToImage(mol)
        return image

    def summary(self):
        mol = self.get_rdkit_mol()
        mol = Chem.RemoveHs(mol)
        from rdkit.Chem import rdMolDescriptors
        print(f"""\
Molecule {rdMolDescriptors.CalcMolFormula(mol)}
SMILES: {Chem.MolToSmiles(mol)}""")
        
        from IPython.display import display
        from rdkit.Chem import Draw
        AllChem.Compute2DCoords(mol)
        display(Draw.MolToImage(mol))

    def generate_xyz(self):
        from rdkit.Chem import AllChem
        mol = self.get_rdkit_mol()
        AllChem.EmbedMolecule(mol)
        AllChem.MMFFOptimizeMolecule(mol)
        xyzs = []
        syms = []
        for i, atom in enumerate(mol.GetAtoms()):
            syms.append(atom.GetSymbol())
            positions = mol.GetConformer().GetAtomPosition(i)
            xyzs.append([positions.x, positions.y, positions.z])
        return xyzs, syms

    @staticmethod
    def item_match(itemA, itemB):
        # all assigned features (bond order, atom element, charge, etc.) must be equal
        feature_set = set(itemA.keys()).intersection(set(itemB.keys()))
        res = True
        for feature in feature_set:
            res = res and (itemA[feature] == itemB[feature])
        return res

    def gen_small_g(self):
        self.smallG = copy.deepcopy(self.G)
        nodes_to_remove = []
        for node in self.smallG.nodes:
            if self.smallG.nodes[node]['sym'] == 'H':
                nodes_to_remove.append(node)
        for node in nodes_to_remove:
            self.smallG.remove_node(node)
        
        self.formula = {}
        for node in self.smallG.nodes:
            if self.smallG.nodes[node]['sym'] not in self.formula:
                self.formula[self.smallG.nodes[node]['sym']] = 0
            self.formula[self.smallG.nodes[node]['sym']] += 1
        
    def __eq__(self, other):
        if not hasattr(self, 'smallG'):
            self.gen_small_g()
        if not hasattr(other, 'smallG'):
            other.gen_small_g()
        
        # Compare simple formulas first
        if set(self.formula.keys()) != set(other.formula.keys()):
            return False
        for elem, idx in self.formula.items():
            if idx != other.formula[elem]:
                return False
        
        return nx.algorithms.isomorphism.is_isomorphic(self.smallG, other.smallG, 
                                node_match=AbstractMolecule.item_match,
                                edge_match=AbstractMolecule.item_match)

    def match_pattern(self, pattern):
        gm = isomorphism.GraphMatcher(self.G, pattern,
                                node_match=AbstractMolecule.item_match,
                                edge_match=AbstractMolecule.item_match)
        return list(gm.subgraph_isomorphisms_iter())

    def get_element_idx(self, elem):
        idx = 0
        for node in self.G.nodes:
            if self.G.nodes[node]['sym'] == elem:
                idx += 1
        return idx

    def get_number_of_cycles(self):
        return self.G.number_of_edges() - self.G.number_of_nodes() + 1
