import networkx as nx
import itertools, multiprocessing, json
from copy import deepcopy
from .abstractmolecule import AbstractMolecule


def get_splitting(a, n):
    k, m = divmod(a, n)
    return [(i*k+min(i, m), (i+1)*k+min(i+1, m)) for i in range(n)]


class IntermediatesListShared:
    def __init__(self, man):
        self.intermediates = man.list()
        self.lock = multiprocessing.Lock()

    def get_lock(self):
        return self.lock.acquire()

    def __len__(self):
        return len(self.intermediates)

    def append(self, new_item):
        self.intermediates.append(deepcopy(new_item))


class IntermediatesPool:
    def __init__(self):
        self.reactions = {}
        self.intermediates = []
        self.reaction_centers = {}
        self.settings = {
            'nthreads': 1,
            'exclude_patterns': [],
            'overlap_policy': 'avoid_collisions', # 'skip_overlaps', 'improvise'
            'accept_criterion': lambda prod: True,
            'associated_products_policy': 'benign' # or 'strict'
        }
    
    @staticmethod
    def partition(collection):
        if len(collection) == 1 or len(collection) == 0:
            yield [ collection ]
            return

        first = collection[0]
        for smaller in IntermediatesPool.partition(collection[1:]):
            # insert `first` in each of the subpartition's subsets
            for n, subset in enumerate(smaller):
                yield smaller[:n] + [[ first ] + subset]  + smaller[n+1:]
            # put `first` in its own subset 
            yield [ [ first ] ] + smaller

    @staticmethod
    def get_molidxs(components, intermediates, settings, atoms_involved, formed_bonds):
        interm_idxs = {}
        for i, comp in enumerate(components):
            if comp['intermediate_idx'] not in interm_idxs:
                interm_idxs[comp['intermediate_idx']] = [i]
            else:
                interm_idxs[comp['intermediate_idx']].append(i)

        partial_assigments = {}
        for interm_idx, idxs in interm_idxs.items():
            if len(idxs) == 1:
                continue
            partial_assigments[interm_idx] = []
            for partition in IntermediatesPool.partition(idxs):
                cur_idxs = {idx: None for idx in idxs}
                abort = False
                for i, subset in enumerate(partition):
                    if settings['overlap_policy'] == 'skip_overlaps':
                        cur_atoms = []
                        for j in subset:
                            for atom in components[j]['center'].keys():
                                if atom in cur_atoms:
                                    abort = True
                                    break
                                cur_atoms.append(atom)
                            if abort:
                                break
                        if abort:
                            break
                    else:
                        # Mandatory check to get reasonable matches
                        cur_atoms = []
                        for j in subset:
                            for atom in components[j]['center'].keys():
                                if components[j]['center'][atom] not in atoms_involved:
                                    pass
                                elif atom not in cur_atoms:
                                    cur_atoms.append(atom)
                                else:
                                    abort = True
                                    break
                            if abort:
                                break

                    if settings['overlap_policy'] == 'avoid_collisions' or settings['overlap_policy'] == 'skip_overlaps':
                        for j, k in zip(subset, subset[1:]):
                            for atA, atA_reag, atB, atB_reag in formed_bonds:
                                if (atA_reag == j and atB_reag == k):# or (atA_reag == k and atB_reag == j):
                                    at_a = components[atA_reag]['center_rev'][atA]
                                    at_b = components[atB_reag]['center_rev'][atB]

                                    if at_a == at_b or intermediates[interm_idx].G.has_edge(at_a, at_b):
                                        abort = True
                                        break
                            if abort:
                                break
                        if abort:
                            break
                    
                    
                    if settings['overlap_policy'] == 'improvise':
                        raise NotImplemented
                        # # The weakest check of all
                        # for atA, atA_reag, atB, atB_reag in formed_bonds:
                        #     at_a = components[atA_reag]['center_rev'][atA]
                        #     at_b = components[atB_reag]['center_rev'][atB]
                        #     if at_a == at_b:
                        #         abort = True
                        #         break
                        # if abort:
                        #     break
                    # Do the job if checks are passed
                    for j in subset:
                        cur_idxs[j] = i
                if abort:
                    continue
                assert None not in cur_idxs.values()
                partial_assigments[interm_idx].append(cur_idxs)
        
        assigments = []
        for combination in itertools.product(*partial_assigments.values()):
            mol_assignment = [None for i in range(len(components))]
            for i, comp in enumerate(components):
                if len(interm_idxs[comp['intermediate_idx']]) == 1:
                    mol_assignment[i] = 0
            for interm_idx, cur_idxs in zip(partial_assigments.keys(), combination):
                for component_idx, cur_idx in cur_idxs.items():
                    mol_assignment[component_idx] = cur_idx
            assert None not in mol_assignment
            assigments.append(mol_assignment)
        return assigments

    def read_reaction_centers(self, im, i):
        for reaction_name, reaction_obj in self.reactions.items():
            all_centers = reaction_obj.check_reactivity(im)
            for reag_type, centers in all_centers.items():
                for center in centers:
                    cur_center = {
                        'intermediate_idx': i,
                        'center': center,
                        'center_rev': {value: key for key, value in center.items()},
                    }
                    self.reaction_centers[reaction_name][reag_type].append(cur_center)

    def find_all(self, print_summary=False, print_progress=True, force_parallel=False, max_generation=-1):
        if self.settings['nthreads'] == 1 and not force_parallel:
            self._find_all_single(print_summary, print_progress, max_generation)
        else:
            self._find_all_parallel(print_summary, print_progress, max_generation)

    def _find_all_single(self, print_summary, print_progress, max_generation):
        for reaction_name, reaction_obj in self.reactions.items():
            self.reaction_centers[reaction_name] = {}
            for reag_type in self.reactions[reaction_name].reagents.keys():
                self.reaction_centers[reaction_name][reag_type] = []

        for i, im in enumerate(self.intermediates):
            self.read_reaction_centers(im, i)

        self.formed_bonds = {}
        self.involved_atoms = {}
        for reaction_name, reaction_obj in self.reactions.items():
            reag_graph = nx.Graph()
            reag_names = list(self.reactions[reaction_name].reagents)
            for reag_name, reag_subgraph in self.reactions[reaction_name].reagents.items():
                reag_graph = nx.compose(reag_graph, reag_subgraph)
                for node in reag_subgraph.nodes:
                    reag_graph.nodes[node]['reagent'] = reag_names.index(reag_name)
            prod_graph = nx.Graph()
            for prod in self.reactions[reaction_name].products.values():
                prod_graph = nx.compose(prod_graph, prod)
            
            self.formed_bonds[reaction_name] = []
            self.involved_atoms[reaction_name] = {} # There might be repeated inclusions so it's a dict
            for atA, atB in prod_graph.edges:
                if not reag_graph.has_edge(atA, atB) or \
                    not AbstractMolecule.item_match(reag_graph[atA][atB], prod_graph[atA][atB]):
                    self.formed_bonds[reaction_name].append((atA, reag_graph.nodes[atA]['reagent'],
                                                                atB, reag_graph.nodes[atB]['reagent']))
                    self.involved_atoms[reaction_name][atA] = reag_graph.nodes[atA]['reagent']
                    self.involved_atoms[reaction_name][atB] = reag_graph.nodes[atB]['reagent']
            for atA, atB in reag_graph.edges:
                if not prod_graph.has_edge(atA, atB):
                    self.involved_atoms[reaction_name][atA] = reag_graph.nodes[atA]['reagent']
                    self.involved_atoms[reaction_name][atB] = reag_graph.nodes[atB]['reagent']

        generation_number = 1
        new_added = True
        while new_added and generation_number != max_generation:
            print(f"Running {generation_number} generation")
            
            if print_progress:
                n_todo = 0
                for reaction_name, reaction_obj in self.reactions.items():
                    for components in itertools.product(*self.reaction_centers[reaction_name].values()):
                        n_todo += 1
                n_done = 0

            new_generation = []    
            for reaction_name, reaction_obj in self.reactions.items():
                for components in itertools.product(*self.reaction_centers[reaction_name].values()):
                    if print_progress:
                        if n_done % 100 == 0:
                            print(f"Done {n_done} out of {n_todo} : {n_done/n_todo*100}%")
                        n_done += 1
                    reagents = {}
                    
                    mol_idxs_sets = IntermediatesPool.get_molidxs(components, self.intermediates, self.settings, self.involved_atoms[reaction_name], self.formed_bonds[reaction_name])
                    for mol_idxs in mol_idxs_sets:
                        for reagent_name, component, mol_idx in zip(reaction_obj.reagents.keys(), components, mol_idxs):
                            reagents[reagent_name] = {
                                'molecule': self.intermediates[component['intermediate_idx']],
                                'center': component['center'],
                                'mol_idx': mol_idx,
                                'intermediate_idx': component['intermediate_idx'],
                            }
                        products = reaction_obj.get_products(reagents, self.settings)
                        new_intermediates = []
                        all_accepted = True
                        for product in products:
                            if not self.settings['accept_criterion'](product):
                                all_accepted = False
                                if self.settings['associated_products_policy'] == 'benign':
                                    continue
                                else: # 'strict'
                                    break
                            
                            if product in self.intermediates or product in new_generation:
                                continue

                            abort = False
                            for pattern in self.settings['exclude_patterns']:
                                if product.match_pattern(pattern):
                                    abort = True
                                    break
                            if abort:
                                all_accepted = False
                                if self.settings['associated_products_policy'] == 'benign':
                                    continue
                                else: # 'strict'
                                    break
                            new_intermediates.append(product)
                        
                        if not all_accepted and self.settings['associated_products_policy'] == 'strict':
                            continue
                        
                        for product in new_intermediates:
                            if print_summary:
                                print("New IM:")
                                reag_smiles = []
                                for reag in reagents.values():
                                    reag_smiles.append(reag['molecule'].get_smiles())
                                print("From: " + " + ".join(reag_smiles))
                                prod_smiles = []
                                for prod in products:
                                    prod_smiles.append(prod.get_smiles())
                                print("Products: " + " + ".join(prod_smiles))
                                product.summary()
                            new_generation.append(product)
                            
            for i, intermediate in enumerate(new_generation):
                self.read_reaction_centers(intermediate, len(self.intermediates) + i)
            new_added = len(new_generation) > 0
            if new_added:
                self.intermediates += new_generation
            generation_number += 1
            print(f"Pool size = {len(self.intermediates)}")
        
    def _find_all_parallel(self, print_summary, print_progress, max_generation):
        for reaction_name, reaction_obj in self.reactions.items():
            self.reaction_centers[reaction_name] = {}
            for reag_type in self.reactions[reaction_name].reagents.keys():
                self.reaction_centers[reaction_name][reag_type] = []

        for i, im in enumerate(self.intermediates):
            self.read_reaction_centers(im, i)

        self.formed_bonds = {}
        self.involved_atoms = {}
        for reaction_name, reaction_obj in self.reactions.items():
            reag_graph = nx.Graph()
            reag_names = list(self.reactions[reaction_name].reagents)
            for reag_name, reag_subgraph in self.reactions[reaction_name].reagents.items():
                reag_graph = nx.compose(reag_graph, reag_subgraph)
                for node in reag_subgraph.nodes:
                    reag_graph.nodes[node]['reagent'] = reag_names.index(reag_name)
            prod_graph = nx.Graph()
            for prod in self.reactions[reaction_name].products.values():
                prod_graph = nx.compose(prod_graph, prod)
            
            self.formed_bonds[reaction_name] = []
            self.involved_atoms[reaction_name] = {} # There might be repeated inclusions so it's a dict
            for atA, atB in prod_graph.edges:
                if not reag_graph.has_edge(atA, atB) or \
                    not AbstractMolecule.item_match(reag_graph[atA][atB], prod_graph[atA][atB]):
                    self.formed_bonds[reaction_name].append((atA, reag_graph.nodes[atA]['reagent'],
                                                                atB, reag_graph.nodes[atB]['reagent']))
                    self.involved_atoms[reaction_name][atA] = reag_graph.nodes[atA]['reagent']
                    self.involved_atoms[reaction_name][atB] = reag_graph.nodes[atB]['reagent']
            for atA, atB in reag_graph.edges:
                if not prod_graph.has_edge(atA, atB):
                    self.involved_atoms[reaction_name][atA] = reag_graph.nodes[atA]['reagent']
                    self.involved_atoms[reaction_name][atB] = reag_graph.nodes[atB]['reagent']
        
        generation_number = 1
        new_added = True
        while new_added and generation_number != max_generation:
            print(f"Running {generation_number} generation")
            
            n_todo = 0
            if print_progress:
                for reaction_name, reaction_obj in self.reactions.items():
                    for components in itertools.product(*self.reaction_centers[reaction_name].values()):
                        n_todo += 1
                n_done = 0

            new_generation = []    
            for reaction_name, reaction_obj in self.reactions.items():
                n_tasks = 0
                for components in itertools.product(*self.reaction_centers[reaction_name].values()):
                    n_tasks += 1

                n_threads = min(n_tasks, self.settings['nthreads'])
                limits = get_splitting(n_tasks, n_threads)
                with multiprocessing.Manager() as manager:
                    new_intermediates = IntermediatesListShared(manager)
                    if print_progress:
                        n_done = multiprocessing.Value('i', n_done)
                    else:
                        n_done = 0
                    jobs = [multiprocessing.Process(target=IntermediatesPool.parallel_worker, name=str(i), 
                                                    args=(deepcopy(reaction_obj),
                                                          deepcopy(self.intermediates),
                                                          deepcopy(limits[i]),
                                                          deepcopy(self.settings),
                                                          deepcopy(self.reaction_centers[reaction_name]),
                                                          new_intermediates,
                                                          n_done,
                                                          n_todo,
                                                          self.formed_bonds[reaction_name],
                                                          self.involved_atoms[reaction_name],
                                                          print_summary,
                                                          print_progress))
                            for i in range(n_threads)]
                    for j in jobs:
                        j.start()
                    for j in jobs:
                        j.join()
                    new_generation += list(new_intermediates.intermediates)
                    if print_progress:
                        n_done = n_done.value
            
            for i, intermediate in enumerate(new_generation):
                self.read_reaction_centers(intermediate, len(self.intermediates) + i)
            new_added = len(new_generation) > 0
            if new_added:
                self.intermediates += new_generation
            generation_number += 1
            print(f"Pool size = {len(self.intermediates)}")

    @staticmethod
    def parallel_worker(reaction_obj, intermediates_inst, limits, settings, reaction_centers, interm_shared,
                        n_done, n_todo, formed_bonds, involved_atoms, print_summary, print_progress):
        iter_idx = 0
        overall_new_intermediates = 0

        for components in itertools.product(*reaction_centers.values()):
            if iter_idx < limits[0] or iter_idx >= limits[1]:
                continue
            iter_idx += 1

            if print_progress:
                with n_done.get_lock():
                    if n_done.value % 100 == 0:
                        print(f"Done {n_done.value} out of {n_todo} : {n_done.value/n_todo*100}%")
                    n_done.value += 1
            reagents = {}

            mol_idxs_sets = IntermediatesPool.get_molidxs(components, intermediates_inst, settings, involved_atoms, formed_bonds)
            for mol_idxs in mol_idxs_sets:
                for reagent_name, component, mol_idx in zip(reaction_obj.reagents.keys(), components, mol_idxs):
                    reagents[reagent_name] = {
                        'molecule': intermediates_inst[component['intermediate_idx']],
                        'center': component['center'],
                        'mol_idx': mol_idx,
                        'intermediate_idx': component['intermediate_idx'],
                    }
                products = reaction_obj.get_products(reagents, settings)
                new_intermediates = []
                all_accepted = True
                lock_used = False
                for product in products:
                    if not settings['accept_criterion'](product):
                        all_accepted = False
                        if settings['associated_products_policy'] == 'benign':
                            continue
                        else: # 'strict'
                            break
                    
                    abort = False
                    for pattern in settings['exclude_patterns']:
                        if product.match_pattern(pattern):
                            abort = True
                            break
                    if abort:
                        all_accepted = False
                        if settings['associated_products_policy'] == 'benign':
                            continue
                        else: # 'strict'
                            break

                    if product in intermediates_inst:
                        continue
                    
                    if not lock_used:
                        interm_shared.lock.acquire()
                        lock_used = True

                    if len(interm_shared) > overall_new_intermediates:
                        already_checked = len(intermediates_inst)
                        intermediates_inst += interm_shared.intermediates[overall_new_intermediates:]
                        overall_new_intermediates = len(interm_shared)
                        if product in intermediates_inst[already_checked:]:
                            continue
                        
                    new_intermediates.append(product)
                
                if not all_accepted and settings['associated_products_policy'] == 'strict':
                    if lock_used:
                        interm_shared.lock.release()
                    continue
                
                for product in new_intermediates:
                    if print_summary:
                        print("New IM:")
                        reag_smiles = []
                        for reag in reagents.values():
                            reag_smiles.append(reag['molecule'].get_smiles())
                        print("From: " + " + ".join(reag_smiles))
                        prod_smiles = []
                        for prod in products:
                            prod_smiles.append(prod.get_smiles())
                        print("Products: " + " + ".join(prod_smiles))
                        product.summary()
                    intermediates_inst.append(product)
                    assert lock_used
                    interm_shared.append(product)
                if lock_used:
                    interm_shared.lock.release()
    
    def show_ensemble(self):
        print(f"The pool consists of {len(self.intermediates)} intermediates")
        for im in self.intermediates:
            im.summary()
